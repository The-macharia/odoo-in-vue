
---
title: "Introduction"
description: "Introduction"
date: 2020-01-11T14:09:21+09:00
---

This project is actually about more than just replacing your QWeb development paradigm with Quasar/Vue. In its entirety,
it also demonstrates how you can setup your Odoo dev project side by side with your Vue dev project. I even provide a
rough guide for getting setup for Odoo development. Why? It's great that this project can help an Odoo developer become
productive quickly with Quasar/Vue, but I also think it can work the other way around. I hope that it might get some 
interested Vue developers introduced to Odoo development.

Please explore this Introduction section further by exploring the sub menus on the left.

---
