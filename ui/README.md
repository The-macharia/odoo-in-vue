**Before you run the Quasar/Vue sub-project follow these steps (as needed).**

Install the dependencies:

`yarn`

Start the app in development mode (hot-code reloading, error reporting, etc.):

`quasar dev`

Lint the files:

`yarn run lint`

Build the app for production:

`quasar build`

Customize the configuration:

See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

---
